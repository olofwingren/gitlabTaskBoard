function Helpers() {
	"use strict";
	
	this.isJson = function(text) {
		var result = /^[\],:{}\s]*$/.test(text.replace(/\\["\\\/bfnrtu]/g, '@').
replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
replace(/(?:^|:|,)(?:\s*\[)+/g, ''));

		if (result) {
			// Check if object when parsed
			if (text === "" || getType(JSON.parse(text)) !== "Object") {
				result = false;
			}
		}
		
		return result;
	}
	
	 function getType(object) {
		var stringConstructor = "test".constructor;
		var arrayConstructor = [].constructor;
		var objectConstructor = {}.constructor;

		if (object === null) {
			return "null";
		}
		else if (object === undefined) {
			return "undefined";
		}
		else if (object.constructor === stringConstructor) {
			return "String";
		}
		else if (object.constructor === arrayConstructor) {
			return "Array";
		}
		else if (object.constructor === objectConstructor) {
			return "Object";
		}
		else {
			return "N/A";
		}
	}
}