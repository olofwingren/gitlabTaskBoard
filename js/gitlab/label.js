function GitlabLabel(parentStoryboard, projectId) {
    "use strict";	
	
	this.fetch = function(name, callback) {
		list(function(labels){
			
			// Find project of type			
			for (var l in labels) {
				var label = labels[l];
				if (label.name === name) {
					callback(label);
					return;
				}
			}
			
			callback(null);
		});
	}
	
	this.list = function(callback) {
		list(callback);
	}
	
	this.addLabelsFromProject = function(otherProjectId, callback) {
		var otherLabel = new GitlabLabel(parentStoryboard, otherProjectId);
		otherLabel.list(function(otherProjectLabels) {
			list(function(labels) {
				for (var i1 in otherProjectLabels) {
					var found = false;
					for (var i2 in labels) {
						if (labels[i2].name === otherProjectLabels[i1].name) {
							found = true;
						}
					}
					
					// add label if not found in iteration
					if (!found) {
						create(otherProjectLabels[i1], function(label){
							console.log(label);
						});
					}
				}
				
				callback();
			});
			
		});
	}
	
	function list(callback) {
		var path = "/projects/" + projectId + "/labels";
		var url = new GitlabServer(parentStoryboard.parameters).url(path);
		
		$.ajax({
			url: url
		}).then(function(labels) {
			callback(labels);
		});
	}
	
	function create(label, callback) {
		var path = "/projects/" + projectId + "/labels";
		path += "?name=" + encodeURIComponent(label.name);
		
		if (label.color === null) {
			label.color = "#fff";
		}
		
		path += "&color=" + encodeURIComponent(label.color);
		
		if (label.description !== null) {
			path += "&description=" + encodeURIComponent(label.description);
		}
		var url = new GitlabServer(parentStoryboard.parameters).url(path);
		
		
		$.ajax({
			type: "post",
			url: url
		}).then(function(data) {
			console.log(data);
			callback(data);
		});
	}
};