function GitlabTask(parentStoryboard) {
    "use strict";
	var gitlabTaskProjectId = null;
	
	var namespacePath = null;
	this.init = function(projectId, namespace, callback) {
		namespacePath = namespace.path;
		var gitlabProject = new GitlabProject(parentStoryboard);
		
		gitlabProject.getTaskProject(namespacePath, function(taskProject){
			gitlabTaskProjectId = taskProject.id;
			// Sync labels
			var gitlabLabel = new GitlabLabel(parentStoryboard, taskProject.id);
			gitlabLabel.addLabelsFromProject(projectId, function(){
				callback();
			});
		});
	}
	
	this.showAddDialog = function(milestoneLabel, label, storyId, callback) {
		$("#modalAddTask").modal("toggle");
		
		$("#modalAddTask button.add").off().on("click", function() {
			var gitlabMilestone = new GitlabMilestone(parentStoryboard, gitlabTaskProjectId);
			gitlabMilestone.fetch(parentStoryboard.milestone, function(milestone){
				var title = $("#task-title").val();
				var description = $("#task-description").val();
				var estimatedHours = $("#task-estimatedHours").val();
				var colorClass = $("#task-color").val();
				var taskData = {
					estimatedHours: estimatedHours,
					storyId: storyId,
					parentTaskId: null,
					class: colorClass
				};
				update(null, title, description, taskData, milestone.id, label, function(task) {
					callback(task);
					$("#modalAddTask").modal("toggle");
				});
			});
		});
	}
	
	this.edit = function(task, callback) {
		update(task.id, task.title, task.description, task.taskData, task.milestone.id, task.labelName, callback);
	}
	
	this.list = function(milestoneTitle, callback) {
		var path = "/projects/" + gitlabTaskProjectId + "/issues";
		var url = new GitlabServer(parentStoryboard.parameters).url(path);
		
		$.ajax({
			url: url
		}).then(function(tasks) {
			
			for (var i in tasks) {
				parseTask(tasks[i]);
			}
			
			callback(tasks);
		});
	}	
	
	this.get = function(taskId, callback) {
		var path = "/projects/" + gitlabTaskProjectId + "/issues";
		path += "?iid=" + taskId;
		var url = new GitlabServer(parentStoryboard.parameters).url(path);
		
		$.ajax({
			url: url
		}).then(function(tasks) {
			parseTask(tasks[0]);
			callback(tasks[0]);
		});
	}
	
	// Parse task description before sending it outside class
	function parseTask(task) {
		var parsed = parseDescription(task.description);
		task.description = parsed.description;
		task.taskData = parsed.data;
	}
	
	//Add or edit a task. Set id to null to add new
	function update(id, title, description, taskData, milestoneId, labelName, callback) {
		
		var path = "/projects/" + gitlabTaskProjectId + "/issues";
		
		var verb = "post";
		if (id !== null) {
			verb = "put";
			path += "/" + id;
		}
		
		var parsed = parseDescription(description);
		parsed.data = taskData;
		
		path += "?title=" + title;
		path += "&milestone_id=" + milestoneId;
		path += "&assignee_id=" + parentStoryboard.gitlabUser().getCurrentUser().id;
		path += "&labels=" + labelName;
		path += "&description=" + encodeURIComponent(setExtendedData(parsed.description, parsed.data));
		var url = new GitlabServer(parentStoryboard.parameters).url(path);
		
		$.ajax({
			type: verb,
			url: url
		}).then(function(task) {
			parseTask(task);
			callback(task);
		});
	}
	
	// The description keeps extra data about which story it's connected to and in which order the task should be displayed in lane
	function parseDescription(description) {
		if (description === null) {
			return {
				description: null,
				data: {storyId: 0}
			};
		}
		var rows = description.split(/\r?\n/);
		var lastRow = rows[rows.length - 1];
		var result = null;
		
		if (new Helpers().isJson(lastRow)) {
			rows.splice(-1,1);
			var concat = rows.join("\r\n");
			result = {
				description: concat,
				data: JSON.parse(lastRow)
			};
		}
		else {
			result = {
				description: description,
				data: {storyId: 0}
			};
		}
		
		return result;
	}
	
	function setExtendedData(description, data) {
		description += "\r\n";
		description += JSON.stringify(data);
		return description;
	}	
};