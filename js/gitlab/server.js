function GitlabServer(parameters) {
	"use strict";
	this.url = function(path) {
		var result = parameters.address + path;
		
		if (parameters.token === null) {
			return result;
		}
		
		if (result.indexOf("?") === -1) {
			result += "?";
		}
		else {
			result += "&";
		}
		result += "private_token=" + parameters.token;
		console.log(result);
		return result;
	}
}