function GitlabMilestone(parentStoryboard, projectId) {
    "use strict";	
	
	this.fetch = function(title, callback) {
		var path = "/projects/" + projectId + "/milestones";
		var url = new GitlabServer(parentStoryboard.parameters).url(path);
		
		$.ajax({
			url: url
		}).then(function(milestones) {
			console.log(milestones);
			
			// Find project of type
			
			for (var m in milestones) {
				var milestone = milestones[m];
				if (milestone.title === title) {
					callback(milestone);
					return;
				}
			}
			
			create(title, callback);
		});
	}
	
	this.create = function(title, callback) {
		create(title, callback);
	}
	
	function create(title, callback) {
		var path = "/projects/" + projectId + "/milestones";
		url += "?title=" + title;				
		
		$.ajax({
			type: "post",
			url: url
		}).then(function(data) {
			console.log(data);
			callback(data);
		});
	}
};