function GitlabUser(parentStoryboard) {
	"use strict";
	var currentUser = null;
	
	this.init = function(callback) {
		fetchCurrentUser(function(user){
			callback();
		});
	}
	
	this.getCurrentUser = function() {
		return currentUser;
	}
	
	function fetchCurrentUser(callback) {
		var path = "/user/";
		var url = new GitlabServer(parentStoryboard.parameters).url(path);
		
		$.ajax({
			url: url
		}).then(function(user) {
			currentUser = user;
			callback(user);
		});
	}
}