function Storyboard(GITLAB, board) {
    "use strict";
	
	var gitlabTask = null; // will be fetched in init
	var gitlabUser = null; // will be fetched in init
	this.parameters = GITLAB;
	this.milestone = GITLAB.milestone; // TODO: Use parameters instead
	this.gitlabUser = function() {
		return gitlabUser;
	}
	this.updateTasks = function() {
		updateTasks();
	}
	
	this.updateTaskPosition = function(taskId, parentId) {
		var storyId = $("#" + taskId).parent().parent().parent().data("story");
		var lane = $("#" + taskId).parent().data("label");
		
		gitlabTask.get(taskId, function(task){
			
			task.labelName = lane;
			
			task.taskData.estimatedHours = 10;
			task.taskData.storyId = storyId;
			
			if (parentId) {
				task.taskData.parentTaskId = parentId;
			}
			// TODO: Update parent
			
			gitlabTask.edit(task, function(){});
		});
		
		
		
	}
	
	this.findParent = function(element) {
		var parent = null;
		var cssClasses = element.attr("class");
		if (element.attr("class")) {
			if (cssClasses.indexOf("tile") >= 0) {
				parent = element;
			} else if (cssClasses.indexOf("lane") >= 0) {
				parent = null; // Should not iterate outside of lane
			} else {
				parent = findParent(element.parent());
			}
		}
		else {
			parent = findParent(element.parent());
		}
		
		return parent;
	}
	
	function updateTasks() {
		
		gitlabTask.list(GITLAB.milestone, function(tasks) {
			$(".lane .tile").remove();
			for (var i in tasks) {
				var task = tasks[i];
				addTaskToStory(task);
			}			
		});
	}
	
	function addTaskToStory(task) {
		var tile = getBoardTile(task, true);
		var addToStoryLane = $('.story[data-story="' + task.taskData.storyId + '"] .lane.' + task.labels[0]);
		$(tile).appendTo(addToStoryLane);
	}

	this.init = function(callback) {
		
		// Show backdrop
		$('<div class="modal-backdrop"></div>').appendTo(document.body);
		
		gitlabTask = new GitlabTask(this);
		var gitlabProject = new GitlabProject(this);
		var count = 2;
		gitlabProject.getProjectById(GITLAB.projectId, function(project) {
			var namespace = project.namespace;
			gitlabTask.init(project.id, namespace, function(){
				count--;
				if (count <= 0) {
					$(".modal-backdrop").remove();
					callback();
				}
				
				create();
			});
		});
		
		gitlabUser = new GitlabUser(this);
		gitlabUser.init(function() {
			count--;
			if (count <= 0) {
				$(".modal-backdrop").remove();
				callback();
			}
		});
	}

	function create() {
		addStories(board, GITLAB.projectId, GITLAB.milestone);
		updateTasks();
	}

	function addStories(board, projectId, milestone) {
		var path = "/projects/"+projectId+"/issues?state=opened&milestone=" + milestone;
		var url = new GitlabServer(GITLAB).url(path);
		
		$.ajax({
			url: url
		}).then(function(stories) {
			console.log(stories);
		   
			for (var s in stories) {
				var story = stories[s];
				$("#stories").append(getStory(story));
			}
		   
			
		   
			addLanes(board, projectId);
		});
	}

	function getStory(task) {
		var story = "";
		story +='	<div class="story" data-story="' + task.iid + '">';
		story +='		<div class="card">' + getBoardTile(task, false) + '</div>';
		story +='		<div class="lanes"></div>';
		story +='	</div>';
		return story;
	}

	function addLanes(board, projectId) {
		var path = "/projects/"+projectId+"/boards";
		var url = new GitlabServer(GITLAB).url(path);
		
		$.ajax({
			url: url
		}).then(function(result) {
			console.log(result);
			var data = result[0];
			
			addLane(board, "Todo");
			for (var l in data.lists) {
				var label = data.lists[l];
				addLane(board, label.label.name);
			}
			
			// Add add task button
			$(board).find(".lanes .lane.Todo .toolbar").each(function(index, element){
				$(element).append('<button class="add">Add task</button>');
			});
			
			$(board).find(".lanes .lane.Todo .toolbar button.add").on("click", function(e){
				var label = $(this).parent().parent().data("label");
				var storyId = $(this).parent().parent().parent().parent().data("story");
				gitlabTask.showAddDialog(GITLAB.milestone, label, storyId, function(task) {
					addTaskToStory(task);
				});
			});
		});	
	}

	function getBoardTile(task, draggable) {

		var tile = "";
		
		var cssClass = "tile";
		if (task.taskData && task.taskData.class) {
			 cssClass += " " + task.taskData.class;
		}
		
		if (draggable) {
			tile += '<div id="' + task.iid + '" class="' + cssClass + '" draggable="true" ondragstart="drag(event)">';
		} else {
			tile += '<div id="' + task.iid + '" class="' + cssClass + '">';
		}
		tile += '<div class="titleContainer">';
		tile += '  <div class="id">#' + task.iid + '</div>';
		tile += '  <div class="title">';
		tile += '    <a href="' + task.web_url + '">' + task.title + '</a>'
		tile += '  </div>';
		tile += '</div>';
		
		if (task.assignee) {
			tile += '<div class="assignee user"><img class="avatar" src="' + task.assignee.avatar_url + '">' + task.assignee.name + '</div>';
		}
		
		tile += '<div></div>';
		tile += '</div>';
		return tile;
	}

	function addLane(board, name) {
		board.find(".titles").append("<div>" + name + "</div>");
		board.find(".story").each(function(index, element){
			$(element).find(".lanes").append(getLane(name, name));
		});
	}

	function getLane(className, label) {
		var lane = "";
		lane += '<div class="lane ' + className + '" data-label=' + label +' ondrop="drop(event)" ondragover="allowDrop(event)">'; 
		lane += '	<div class="toolbar"></div>';
		lane += '</div>';
		return lane;
	}
}
